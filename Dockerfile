FROM python:3.10-bullseye

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "executable" ]

WORKDIR /usr/src/app

COPY ./requirements.txt .

RUN pip install -r /usr/src/app/requirements.txt

COPY . .


EXPOSE 3000

CMD ["uvicorn", "config:app", "--host=127.0.0.1", "--port=3000"]

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# FROM python:latest

# RUN sudo apt update 

# RUN sudo apt upgrade

# RUN sudo echo "${USER}:pass" | chpasswd

# WORKDIR /usr/src/app

# COPY . .

# RUN curl -sSL https://install.python-poetry.org | python3 -

# ENV PATH="${PATH}:/root/.local/share/pypoetry/venv/bin/"
# RUN ["poetry", "install", "--no-dev"]

# EXPOSE 5003 22 443 8080 20

# CMD ["poetry", "run", "start"]